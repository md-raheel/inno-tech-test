import React from "react";
import { Input } from "reactstrap";

const SearchProduct = ({ value, onChange }) => {
  return (
    <div>
      <Input
        value={value}
        onChange={onChange}
        placeholder="Search products by name"
      />
    </div>
  );
};

export default SearchProduct;
