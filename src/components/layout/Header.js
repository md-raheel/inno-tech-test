import React from "react";
import { useHistory, useLocation } from "react-router-dom";
import { Navbar, NavbarBrand, NavLink, Container } from "reactstrap";

const Header = () => {
  const history = useHistory();
  const location = useLocation();

  const renderLinkBtn = (btnText, route) => (
    <NavLink
      className="btn btn-light ml-auto"
      onClick={() => history.push(route)}
    >
      {btnText}
    </NavLink>
  );

  return (
    <Navbar className="navbar-dark bg-primary">
      <Container>
        <NavbarBrand href="#" className="mr-auto">
          Product List
        </NavbarBrand>
        {location.pathname === "/" &&
          renderLinkBtn("Add Product", "/product/add")}
        {location.pathname === "/product/add" &&
          renderLinkBtn("Return home", "/")}
      </Container>
    </Navbar>
  );
};

export default Header;
