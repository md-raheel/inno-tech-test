import React from "react";
import img from "../../assets/img/sample.svg";
import { useHistory } from "react-router-dom";
import { FaRegCalendarAlt, FaEdit } from "react-icons/fa";
import { Card, CardImg, CardBody, CardTitle, CardSubtitle } from "reactstrap";

const ProductCard = ({ id, name, description, price, inventory_date }) => {
  const history = useHistory();
  return (
    <Card className="product-card shadow bg-white rounded">
      <div className="d-flex">
        <CardImg top width="100%" src={img} alt="Card image cap" />
        <span
          className="edit-action"
          onClick={() => history.push(`/product/edit/${id}`)}
        >
          <FaEdit style={{ color: "#e83e8c" }} />
        </span>
      </div>
      <CardBody className="pb-5">
        <div className="d-flex justify-content-between align-middle">
          <CardTitle tag="h5">{name}</CardTitle>
          <div className="text-primary">&#36; {price}</div>
        </div>
        <CardSubtitle tag="h6" className="mb-2 text-muted">
          {description}
        </CardSubtitle>
        {inventory_date && (
          <div style={{ position: "absolute", right: 12, bottom: 1 }}>
            <div className="d-flex">
              <FaRegCalendarAlt />
              <p style={{ marginTop: -2, marginLeft: 3 }}>{inventory_date}</p>
            </div>
          </div>
        )}
      </CardBody>
    </Card>
  );
};

export default ProductCard;
