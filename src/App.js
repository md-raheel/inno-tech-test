import "./style.css";
import React from "react";
import Home from "./pages/Home";
import store from "./state/store";
import { Provider } from "react-redux";
import AddProduct from "./pages/AddProduct";
import Header from "./components/layout/Header";

import { BrowserRouter as Router, Route } from "react-router-dom";

const App = () => {
  return (
    <>
      <Provider store={store}>
        <Router>
          <Header />
          <Route exact path="/" component={Home} />
          <Route exact path="/product/add" component={AddProduct} />
          <Route exact path="/product/edit/:id" component={AddProduct} />
        </Router>
      </Provider>
    </>
  );
};

export default App;
