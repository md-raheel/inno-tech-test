import { CREATE_PRODUCT, GET_PRODUCT, UPDATE_PRODUCT } from "./constant";

const initialState = {
  products: [
    {
      id: "1",
      name: "Traditional Dress",
      description: "Unique white traditional long dress",
      price: "749",
      inventory_date: "2020-08-23",
    },
    {
      id: "2",
      name: "Denim jacket",
      description: "Classic style for classic men",
      price: "456",
      inventory_date: "2021-03-07",
    },
    {
      id: "3",
      name: "Hush Puppies",
      description: "Comfort, style and fun altogether",
      price: "299",
      inventory_date: "2021-01-04",
    },
  ],
  product: null,
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_PRODUCT:
      return {
        ...state,
        products: [action.payload, ...state.products],
      };

    case GET_PRODUCT:
      let productObj = state.products.find(({ id }) => id === action.payload);
      return {
        ...state,
        product: productObj,
      };

    case UPDATE_PRODUCT:
      return {
        ...state,
        products: state.products.map((product) =>
          product.id === action.payload.id ? action.payload : product
        ),
      };

    default:
      return state;
  }
};
