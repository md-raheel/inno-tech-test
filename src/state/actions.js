import { CREATE_PRODUCT, GET_PRODUCT, UPDATE_PRODUCT } from "./constant";

export const addProduct = (product) => {
  return {
    type: CREATE_PRODUCT,
    payload: product,
  };
};

export const getProduct = (id) => {
  return {
    type: GET_PRODUCT,
    payload: id,
  };
};

export const updateProduct = (product) => {
  return {
    type: UPDATE_PRODUCT,
    payload: product,
  };
};
