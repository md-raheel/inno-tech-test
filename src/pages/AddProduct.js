import {
  Card,
  Input,
  Label,
  Button,
  CardBody,
  FormGroup,
  CardHeader,
  Container,
} from "reactstrap";
import * as Yup from "yup";
import shortid from "shortid";
import React, { useEffect } from "react";
import { Form, Field, Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router-dom";
import { addProduct, getProduct, updateProduct } from "../state/actions";

const AddProduct = () => {
  const { id } = useParams();
  const isAddMode = !id;

  const history = useHistory();
  const dispatch = useDispatch();
  const productData = useSelector(({ product }) => product);

  const initialValues = {
    name: "",
    price: "",
    description: "",
    inventory_date: "",
  };

  const validationSchema = Yup.object().shape({
    name: Yup.string().required(),
    price: Yup.number().required(),
  });

  // eslint-disable-next-line
  useEffect(() => dispatch(getProduct(id)), []);

  return (
    <Container>
      <Card className="mt-3 shadow bg-white rounded">
        <CardHeader>{isAddMode ? "Add Product" : "Update Product"} </CardHeader>
        <CardBody>
          <Formik
            enableReinitialize
            initialValues={isAddMode ? initialValues : productData}
            validationSchema={validationSchema}
            onSubmit={(value) => {
              isAddMode
                ? dispatch(addProduct({ id: shortid.generate(), ...value }))
                : dispatch(updateProduct(value));
              history.push("/");
            }}
          >
            {({ errors, touched }) => (
              <Form>
                <FormGroup>
                  <Label for="name">Name</Label>
                  <Field
                    id="name"
                    name="name"
                    type="text"
                    as={Input}
                    placeholder="Enter product name"
                    className={
                      errors.name && touched.name
                        ? "form-control is-invalid"
                        : "form-control"
                    }
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="description">Description</Label>
                  <Field
                    as={Input}
                    type="text"
                    id="description"
                    name="description"
                    placeholder="Enter product description"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="price">Price</Label>
                  <Field
                    as={Input}
                    id="price"
                    name="price"
                    type="number"
                    placeholder="Enter product price"
                    className={
                      errors.price && touched.price
                        ? "form-control is-invalid"
                        : "form-control"
                    }
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="inventory_date">Inventory Date</Label>
                  <Field
                    as={Input}
                    type="date"
                    name="inventory_date"
                    id="inventory_date"
                    placeholder="Enter product inventory_date"
                  />
                </FormGroup>
                <Button className="bg-primary" type="submit">
                  {isAddMode ? "Submit" : "Update"}
                </Button>
              </Form>
            )}
          </Formik>
        </CardBody>
      </Card>
    </Container>
  );
};

export default AddProduct;
