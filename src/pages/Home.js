import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Container, Col, Row } from "reactstrap";
import SearchProduct from "../util/SearchProduct";
import ProductCard from "../components/card/ProductCard";

const Home = () => {
  const [filter, setFilter] = useState("");
  const productList = useSelector(({ products }) => products);

  let filteredData = productList.filter((item) => {
    return Object.keys(item).some((key) =>
      item[key].toLowerCase().includes(filter.toLowerCase())
    );
  });

  return (
    <Container className="mt-5">
      <div className="my-4">
        <SearchProduct
          value={filter}
          onChange={(event) => setFilter(event.target.value)}
        />
      </div>
      <Row>
        {filteredData &&
          filteredData.map(
            ({ id, name, description, price, inventory_date }) => (
              <Col lg={3} md={4} sm={6}>
                <ProductCard
                  id={id}
                  key={id}
                  name={name}
                  price={price}
                  description={description}
                  inventory_date={inventory_date}
                />
              </Col>
            )
          )}
      </Row>
    </Container>
  );
};

export default Home;
